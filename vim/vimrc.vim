set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
" set rtp+=~/dotfiles/vim/.vim/bundle/Vundle.vim
" call vundle#begin()
" Plugin 'VundleVim/Vundle.vim'
" Plugin 'fatih/vim-go'
" Plugin 'ervandew/supertab'

" call vundle#end()            " required
filetype plugin indent on    " required

" General Vim settings
"
	syntax on
	colorscheme molokai
	let g:rainbow_active = 1
	let mapleader=","
  set termguicolors
	set autoindent
	set tabstop=2
	set shiftwidth=2
	set expandtab
	set noswapfile
	set dir=/tmp/
	set relativenumber 
	set number
	set list
	set listchars=eol:⏎,tab:␉·,trail:␠,nbsp:⎵
    set foldmethod=indent

	set cursorline
	hi Cursor ctermfg=White ctermbg=Yellow cterm=bold guifg=white guibg=yellow gui=bold

	set hlsearch
	nnoremap <C-l> :nohl<CR><C-l>:echo "Search Cleared"<CR>
	nnoremap <C-c> :set norelativenumber<CR>:set nonumber<CR>:echo "Line numbers turned off."<CR>
	nnoremap <C-n> :set relativenumber<CR>:set number<CR>:echo "Line numbers turned on."<CR>

	nnoremap n nzzzv
	nnoremap N Nzzzv

	nnoremap H 0
	nnoremap L $
	nnoremap J G
	nnoremap K gg

	map <tab> %

	set backspace=indent,eol,start

	nnoremap <Space> za
	nnoremap <leader>z zMzvzz

	nnoremap vv 0v$

	nnoremap <leader><tab> :set list!<cr>
 "set pastetoggle=<F2>
	set mouse=a
	set incsearch

 "Set paste mode
  "set pastetoggle=<C-j> 

 " Default Key maps
    inoremap { {}<Esc>i
    inoremap " ""<Esc>i
    inoremap [ []<Esc>i
    inoremap ( ()<Esc>i
	inoremap AA <Esc>A
	inoremap jj <Esc>
    inoremap jkjk <C-x><C-o>


" Language Specific
	" General
		inoremap <leader>for <esc>Ifor (int i = 0; i < <esc>A; i++) {<enter>}<esc>O<tab>
		inoremap <leader>if <esc>Iif (<esc>A) {<enter>}<esc>O<tab>
		

	" Java
		inoremap <leader>sys <esc>ISystem.out.println(<esc>A);
		vnoremap <leader>sys yOSystem.out.println(<esc>pA);

	" Java
		inoremap <leader>con <esc>Iconsole.log(<esc>A);
		vnoremap <leader>con yOconsole.log(<esc>pA);

	" C++
		inoremap <leader>cout <esc>Istd::cout << <esc>A << std::endl;
		vnoremap <leader>cout yOstd::cout << <esc>pA << std:endl;

	" C
		inoremap <leader>out <esc>Iprintf(<esc>A);<esc>2hi
		vnoremap <leader>out yOprintf(, <esc>pA);<esc>h%a

	" Typescript
		autocmd BufNewFile,BufRead *.ts set syntax=javascript
		autocmd BufNewFile,BufRead *.tsx set syntax=javascript

	" Markup
		inoremap <leader>< <esc>I<<esc>A><esc>yypa/<esc>O<tab>
	
	" Go
	    
    	let g:go_highlight_types = 1
    	let g:go_highlight_functions = 1
    	let g:go_highlight_methods = 1
        let g:SuperTabDefaultCompletionType = "context"
      au filetype go inoremap <buffer> . .<C-x><C-o>

" File and Window Management 
	inoremap <leader>w <Esc>:w<CR>
	nnoremap <leader>w :w<CR>

	inoremap <leader>q <ESC>:q<CR>
	nnoremap <leader>q :q<CR>

	inoremap <leader>x <ESC>:x<CR>
	nnoremap <leader>x :x<CR>

	nnoremap <leader>e :Ex<CR>
	nnoremap <leader>t :tabnew<CR>:Ex<CR>
	nnoremap <leader>v :vsplit<CR>:w<CR>:Ex<CR>
	nnoremap <leader>s :split<CR>:w<CR>:Ex<CR>

" Return to the same line you left off at
	augroup line_return
		au!
		au BufReadPost *
			\ if line("'\"") > 0 && line("'\"") <= line("$") |
			\	execute 'normal! g`"zvzz' |
			\ endif
	augroup END

  " Go specific
  let g:go_highlight_fields = 1
  let g:go_highlight_functions = 1
  let g:go_highlight_function_calls = 1
  let g:go_highlight_extra_types = 1
  let g:go_highlight_operators = 1
  
  " Auto formatting and importing
  let g:go_fmt_autosave = 1
  let g:go_fmt_command = "goimports"
  
  " Status line types/signatures
  let g:go_auto_type_info = 1
  
  " Run :GoBuild or :GoTestCompile based on the go file
  function! s:build_go_files()
    let l:file = expand('%')
    if l:file =~# '^\f\+_test\.go$'
      call go#test#Test(0, 1)
    elseif l:file =~# '^\f\+\.go$'
      call go#cmd#Build(0)
    endif
  endfunction
  
  " Map keys for most used commands.
  " Ex: `\b` for building, `\r` for running and `\b` for running test.
  autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>
  autocmd FileType go nmap <leader>r  <Plug>(go-run)
  autocmd FileType go nmap <leader>t  <Plug>(go-test)

" Future stuff
	"Swap line
	"Insert blank below and above
