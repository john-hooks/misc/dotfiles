#!/usr/bin/env bash

cp starship.toml ~/.config/starship.toml

ln -sf $(pwd)/zsh/zshrc ~/.zshrc
ln -sf $(pwd)/vim/vimrc.vim ~/.vimrc
ln -sf $(pwd)/vim/.vim ~/.vim
